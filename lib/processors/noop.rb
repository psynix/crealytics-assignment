# Null-object processor; does nothing except return input
module Processors
  class Noop
    def process(input)
      input
    end
  end
end

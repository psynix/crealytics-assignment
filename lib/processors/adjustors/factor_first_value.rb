require 'helpers/german_number_conversion'

module Processors
  module Adjustors
    module FactorFirstValue
      using Helpers::GermanNumberConversion

      module_function def adjust(values, factor: 1)
        (factor * values.first.from_german_to_f).to_german_s
      end
    end
  end
end

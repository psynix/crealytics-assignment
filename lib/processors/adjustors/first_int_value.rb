module Processors
  module Adjustors
    module FirstIntValue
      module_function def adjust(values)
        values.first.to_s
      end
    end
  end
end

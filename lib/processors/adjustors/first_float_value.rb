require 'helpers/german_number_conversion'

module Processors
  module Adjustors
    module FirstFloatValue
      using Helpers::GermanNumberConversion

      module_function def adjust(values)
        values.first.from_german_to_f.to_german_s
      end
    end
  end
end

module Processors
  module Adjustors
    module LastValue
      module_function def adjust(values)
        values.last
      end
    end
  end
end

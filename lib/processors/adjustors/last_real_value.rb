module Processors
  module Adjustors
    module LastRealValue
      module_function def adjust(values)
        values.reject { |value| value.nil? || value == 0 || value == '0' || value == '' }.last
      end
    end
  end
end

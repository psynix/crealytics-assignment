# Groups values by column key
module Processors
  class ColumnValueGrouper
    def process(csv_rows)
      keys = csv_rows.reject(&:nil?).flat_map(&:headers)
      keys.each_with_object({}) do |key, result|
        result[key] = csv_rows.map { |row| row.fetch(key, nil) }
      end
    end
  end
end

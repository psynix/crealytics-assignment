require_relative 'adjustors'

# FIXME: This should really be split out into individual processors by strategy ie. last wins etc.
module Processors
  class PerformanceDataValueAdjustor
    LAST_VALUE_WINS      = ['Account ID', 'Account Name', 'Campaign', 'Ad Group', 'Keyword', 'Keyword Type', 'Subid', 'Paused', 'Max CPC', 'Keyword Unique ID', 'ACCOUNT', 'CAMPAIGN', 'BRAND', 'BRAND+CATEGORY', 'ADGROUP', 'KEYWORD']
    LAST_REAL_VALUE_WINS = ['Last Avg CPC', 'Last Avg Pos']
    INT_VALUES           = ['Clicks', 'Impressions', 'ACCOUNT - Clicks', 'CAMPAIGN - Clicks', 'BRAND - Clicks', 'BRAND+CATEGORY - Clicks', 'ADGROUP - Clicks', 'KEYWORD - Clicks']
    FLOAT_VALUES         = ['Avg CPC', 'CTR', 'Est EPC', 'newBid', 'Costs', 'Avg Pos']

    CANCELLATION_FACTOR_VALUES = ['number of commissions']
    SALEAMOUNT_FACTOR_VALUES   = ['Commission Value', 'ACCOUNT - Commission Value', 'CAMPAIGN - Commission Value', 'BRAND - Commission Value', 'BRAND+CATEGORY - Commission Value', 'ADGROUP - Commission Value', 'KEYWORD - Commission Value']

    def initialize(**options)
      @saleamount_factor   = options[:saleamount_factor] || 1
      @cancellation_factor = options[:cancellation_factor] || 1
    end

    def process(input_hash)
      result = {}

      LAST_VALUE_WINS.each do |key|
        result[key] = Adjustors::LastValue.adjust(input_hash[key])
      end

      LAST_REAL_VALUE_WINS.each do |key|
        result[key] = Adjustors::LastRealValue.adjust(input_hash[key])
      end

      INT_VALUES.each do |key|
        result[key] = Adjustors::FirstIntValue.adjust(input_hash[key])
      end

      FLOAT_VALUES.each do |key|
        result[key] = Adjustors::FirstFloatValue.adjust(input_hash[key])
      end

      CANCELLATION_FACTOR_VALUES.each do |key|
        result[key] = Adjustors::FactorFirstValue.adjust(input_hash[key], factor: cancellation_factor)
      end

      SALEAMOUNT_FACTOR_VALUES.each do |key|
        result[key] = Adjustors::FactorFirstValue.adjust(input_hash[key], factor: cancellation_factor * saleamount_factor)
      end

      result
    end

    private

    attr_reader :saleamount_factor, :cancellation_factor
  end
end

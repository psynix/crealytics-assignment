module Enumerators
  class Merger
    def initialize(*processors)
      @processors = processors
    end

    def merge(enumerator)
      Enumerator.new do |yielder|
        while true
          begin
            processed_value = processors.reduce(enumerator.next) do |result, processor|
              processor.process(result)
            end

            yielder.yield(processed_value)
          rescue StopIteration
            break
          end
        end
      end
    end

    private

    attr_reader :processors
  end
end

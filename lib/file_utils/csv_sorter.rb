require 'defaults'
require 'csv'
require 'pathname'

module FileUtils
  class CsvSorter
    def initialize(input_path, sort_by:, **options)
      @input_path = Pathname(input_path)

      @sort_by           = sort_by
      @csv_read_options  = options[:csv_read_options] || Defaults::CSV_READ_OPTIONS
      @csv_write_options = options[:csv_read_options] || Defaults::CSV_WRITE_OPTIONS
    end

    # Write sorted table to CSV file
    #
    # @return CSV instance
    def write(output_path, extension: 'sorted')
      output_pathname = Pathname(output_path)
      sorted_output_path = output_pathname.sub_ext("#{output_pathname.extname}.#{extension}")
      CSV.open(sorted_output_path, 'wb', csv_write_options) do |csv|
        csv << input_headers
        sorted_rows.each { |row| csv << row }
      end
    end

    private

    attr_reader :input_path, :csv_read_options, :csv_write_options, :sort_by

    def input_data
      @_input_data ||= CSV.read(input_path, csv_read_options)
    end

    def input_headers
      input_data.headers
    end

    def sorted_rows
      input_data.sort_by { |a| -a[sort_key_index].to_i }
    end

    def sort_key_index
      @sort_key_index = input_headers.index(sort_by) || 0
    end
  end
end

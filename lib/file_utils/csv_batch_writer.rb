require 'csv'
require 'pathname'

require 'defaults'

module FileUtils
  class CsvBatchWriter
    LINES_PER_FILE = 120000

    def initialize(input_data)
      @input_data = input_data.to_enum
    end

    def write(path, batch_size: LINES_PER_FILE)
      extension = Pathname(path).extname

      done       = false
      file_index = 0
      until done do
        output_path = Pathname(path).sub_ext("_#{file_index}#{extension}")
        CSV.open(output_path, 'wb', Defaults::CSV_WRITE_OPTIONS) do |csv|
          headers_written = false
          line_count      = 0

          while line_count < batch_size
            begin
              row = input_data.next

              unless headers_written
                csv << row.keys
                headers_written = true
                line_count      += 1
              end

              csv << row
              line_count += 1
            rescue StopIteration
              done = true
              break
            end
          end

          file_index += 1
        end
      end
    end

    private

    attr_reader :input_data
  end
end

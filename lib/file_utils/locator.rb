require 'date'
require 'pathname'

module FileUtils
  class Locator
    def initialize(base_path: "#{ENV['HOME']}/workspace")
      @base_path = Pathname(base_path)
    end

    def latest(file_pattern)
      files = Dir["#{base_path}/*#{file_pattern}*.txt"]

      files.sort_by! do |file|
        last_date = /\d+-\d+-\d+_[[:alpha:]]+\.txt$/.match(file)
        last_date = last_date.to_s.match(/\d+-\d+-\d+/)

        DateTime.parse(last_date.to_s)
      end

      fail RuntimeError if files.empty?

      Pathname(files.last)
    end

    private

    attr_reader :base_path

    class << self
      def latest(file_pattern, base_path: "#{ENV['HOME']}/workspace")
        new.latest(file_pattern)
      end
    end
  end
end

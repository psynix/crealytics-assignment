module Defaults
  CSV_READ_OPTIONS  = { col_sep: "\t", headers: :first_row }
  CSV_WRITE_OPTIONS = { col_sep: "\t", headers: :first_row, row_sep: "\r\n" }
end

require 'enumerators'
require 'file_utils'
require 'processors'

class DataAggregator
  KEYWORD_UNIQUE_ID = 'Keyword Unique ID'

  def initialize(**options)
    @processors = Array(options[:process_with])
  end

  def aggregate(input_path, output_path)
    sorter = FileUtils::CsvSorter.new(input_path, sort_by: 'Clicks')
    sorted_csv = sorter.write(output_path, extension: 'sorted')

    combined = Enumerators::Combiner.new { |value| value[KEYWORD_UNIQUE_ID] }.combine(sorted_csv.to_enum)
    merger   = Enumerators::Merger.new(*processors).merge(combined)

    FileUtils::CsvBatchWriter.new(merger).write(output_path)
  end

  private

  attr_reader :processors
end

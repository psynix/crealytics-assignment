module Helpers
  module GermanNumberConversion
    refine String do
      def from_german_to_f
        gsub(',', '.').to_f
      end
    end

    refine Float do
      def to_german_s
        to_s.gsub('.', ',')
      end
    end
  end
end

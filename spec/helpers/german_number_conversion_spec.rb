require 'helpers/german_number_conversion'

module Helpers
  class TestClass
    using GermanNumberConversion

    def initialize(value)
      @value = value
    end

    def to_float
      @value.from_german_to_f
    end

    def to_string
      @value.to_german_s
    end
  end

  RSpec.describe GermanNumberConversion do
    subject(:refined_object) { TestClass.new(value) }

    describe '#from_german_to_f' do
      subject(:result) { refined_object.to_float }

      context 'when the value empty' do
        let(:value) { '' }

        it 'returns 0.0' do
          is_expected.to equal(0.0)
        end
      end

      context 'when the value is an integer' do
        let(:value) { '5' }

        it 'returns 5.0' do
          is_expected.to equal(5.0)
        end
      end

      context 'when the value is a float' do
        let(:value) { '12.4' }

        it 'returns 12.4' do
          is_expected.to equal(12.4)
        end
      end

      context 'when the value is a German float' do
        let(:value) { '10,3' }

        it 'returns 10.3' do
          is_expected.to equal(10.3)
        end
      end
    end

    describe '#to_german_s' do
      subject(:result) { refined_object.to_string }

      context 'when the value is an integer' do
        let(:value) { 123.45 }

        it 'returns 123,45' do
          is_expected.to eql('123,45')
        end
      end
    end
  end
end

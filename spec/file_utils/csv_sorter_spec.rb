require 'file_utils/csv_sorter'

require 'defaults'

module FileUtils
  RSpec.describe CsvSorter do
    let(:csv_fixture_path) { Pathname.new('spec/fixtures/project_2012-07-27_2012-10-10_performancedata.txt') }

    subject(:csv_sorter) { described_class.new(csv_fixture_path, sort_by: 'Clicks') }

    describe '#write' do
      let(:output_filename)     { Tempfile.new(['sorted_csv_', '.txt']).path }
      let(:sorted_csv_filename) { "#{output_filename}.sorted" }
      let(:csv_contents)        { CSV.read(sorted_csv_filename, Defaults::CSV_READ_OPTIONS) }

      subject(:do_write) { csv_sorter.write(output_filename) }

      before do
        do_write
      end

      after do
        FileUtils.remove_entry(output_filename) # Cleanup output file
      end

      describe 'first row' do
        let(:first_row) { csv_contents[0] }
        let(:clicks)    { first_row['Clicks'].to_i }

        it 'places the account ID with the most clicks first' do
          expect(first_row['Account ID']).to eq('ABCDE005')
        end

        it 'places the largest account by clicks at the beginning' do
          expect(clicks).to be >= csv_contents[1..-1].map { |row| row['Clicks'].to_i }.max
        end
      end

      describe 'last row' do
        let(:last_row) { csv_contents[-1] }
        let(:clicks)   { last_row['Clicks'].to_i }

        it 'places the account ID with the least clicks last' do
          expect(last_row['Account ID']).to eq('ABCDE001')
        end

        it 'places the account with least clicks last' do
          expect(clicks).to be <= csv_contents[0..-2].map { |row| row['Clicks'].to_i }.min
        end
      end
    end
  end
end

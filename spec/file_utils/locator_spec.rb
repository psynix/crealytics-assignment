require 'file_utils/locator'

require 'pathname'
require 'tmpdir'

module FileUtils
  RSpec.describe Locator do
    let(:base_path) { Pathname(Dir.tmpdir) }

    subject(:file_fetcher) { described_class.new(base_path: base_path) }

    describe '.latest(pattern)' do
      let(:latest_file_pattern) { 'project_2018-05-10_2018-05-20_performancedata' }

      subject(:latest_file) { file_fetcher.latest(latest_file_pattern) }

      context 'when no files match the given pattern' do
        it 'raises a RuntimeError' do
          expect { latest_file }.to raise_error(RuntimeError)
        end
      end

      context 'when one file matches the given pattern' do
        let(:latest_matching_path) { base_path.join("#{latest_file_pattern}.txt") }

        around do |example|
          FileUtils.touch(latest_matching_path)
          example.run
          FileUtils.rm(latest_matching_path)
        end

        it 'returns the matched file' do
          expect(latest_file).to eql(latest_matching_path)
        end

        context 'and there are also older files' do
          let(:older_file_pattern)      { 'project_2012-07-27_2012-10-10_performancedata' }
          let(:older_matching_path) { base_path.join("#{older_file_pattern}.txt") }

          around do |example|
            FileUtils.touch(older_matching_path)
            example.run
            FileUtils.rm(older_matching_path)
          end

          it 'returns the matched file' do
            expect(latest_file).to eql(latest_matching_path)
          end
        end
      end
    end
  end
end

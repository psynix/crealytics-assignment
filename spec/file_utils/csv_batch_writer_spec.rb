require 'file_utils/csv_batch_writer'

require 'pathname'
require 'tempfile'

module FileUtils
  RSpec.describe CsvBatchWriter do
    let(:input_data) do
      (1..50).map do
        Hash[columnA: rand(1000), columnB: rand(1000)]
      end
    end

    subject(:writer) { described_class.new(input_data) }

    describe '#write' do
      let(:tempdir)             { Dir.mktmpdir }
      let(:base_path)           { Pathname(tempdir) }
      let(:path)                { base_path + 'csv_batch_file' }
      let(:path_with_extension) { path.sub_ext('.csv') }

      after do
        FileUtils.remove_entry(tempdir)
      end

      context 'when the batch size is not specified' do
        before do
          writer.write(path_with_extension)
        end

        it 'writes one file' do
          expect(Dir["#{path}_*"].size).to be(1)
        end
      end

      context 'when the batch size is set' do
        before do
          writer.write(path_with_extension, batch_size: 10)
        end

        it 'writes one file' do
          expect(Dir["#{path}*"].size).to be(6)
        end
      end
    end
  end
end

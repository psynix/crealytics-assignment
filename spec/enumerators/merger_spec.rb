require 'enumerators/merger'

module Enumerators
  RSpec.describe Merger do
    subject(:merger) { described_class.new(*processors) }

    class DoubleProcessor
      def process(value)
        value * 2
      end
    end

    class GreaterThanFourProcessor
      def process(value)
        value > 4
      end
    end

    describe '#merge' do
      let(:collection) { [1, 2, 3, 4] }
      let(:enumerator) { collection.to_enum }

      subject(:merge_result) { merger.merge(enumerator) }

      context 'when there are no processors' do
        let(:processors) { [] }

        it 'does not modify the enumeration data' do
          is_expected.to return_elements(*collection)
        end
      end

      context 'when there is one processor' do
        let(:processors) { [DoubleProcessor.new] }

        it 'processes the values' do
          is_expected.to return_elements(2, 4, 6, 8)
        end
      end

      context 'when there are many processors' do
        let(:processors) { [DoubleProcessor.new, GreaterThanFourProcessor.new] }

        it 'runs the processors in order' do
          is_expected.to return_elements(false, false, true, true)
        end
      end
    end
  end
end

$:.unshift File.expand_path('../lib/', File.dirname(__FILE__))

RSpec.configure do |config|
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.example_status_persistence_file_path = "spec/examples.txt"

  config.shared_context_metadata_behavior = :apply_to_host_groups
  config.disable_monkey_patching!

  config.warnings = true

  config.profile_examples = 5
  config.order = :random
  Kernel.srand config.seed
end

# Load custom matchers
Dir[File.dirname(__FILE__) + "/support/**/*.rb"].each { |f| require f }

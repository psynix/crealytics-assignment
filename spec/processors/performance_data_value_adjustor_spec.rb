require 'processors/performance_data_value_adjustor'
require 'helpers/german_number_conversion'

module Processors
  using Helpers::GermanNumberConversion

  RSpec.describe PerformanceDataValueAdjustor do
    let(:all_columns) do
      PerformanceDataValueAdjustor::FLOAT_VALUES + PerformanceDataValueAdjustor::SALEAMOUNT_FACTOR_VALUES + PerformanceDataValueAdjustor::CANCELLATION_FACTOR_VALUES
    end

    def data_for_last_value_wins
      PerformanceDataValueAdjustor::LAST_VALUE_WINS.each_with_object({}) do |column_name, result|
        result[column_name] = ['first value', 'last value']
      end
    end

    def data_for_last_real_value_wins
      PerformanceDataValueAdjustor::LAST_REAL_VALUE_WINS.each_with_object({}) do |column_name, result|
        result[column_name] = ['first value', nil, 0, '', 'last value', nil, 0, '']
      end
    end

    def data_for_int_values
      PerformanceDataValueAdjustor::INT_VALUES.each_with_object({}) do |column_name, result|
        result[column_name] = [12, 34, 56, 78]
      end
    end

    def data_for_float_values
      PerformanceDataValueAdjustor::FLOAT_VALUES.each_with_object({}) do |column_name, result|
        result[column_name] = %w(12,34 56,78 90,12)
      end
    end

    def data_for_saleamout_factor_values
      PerformanceDataValueAdjustor::SALEAMOUNT_FACTOR_VALUES.each_with_object({}) do |column_name, result|
        result[column_name] = %w(24,17 56,73)
      end
    end

    def data_for_cancellation_factor_values
      PerformanceDataValueAdjustor::CANCELLATION_FACTOR_VALUES.each_with_object({}) do |column_name, result|
        result[column_name] = %w(10,13 12,0)
      end
    end

    let(:saleamount_factor)   { 1.25 }
    let(:cancellation_factor) { 0.3 }

    subject(:processor) { described_class.new(cancellation_factor: cancellation_factor, saleamount_factor: saleamount_factor) }

    describe '#process' do
      let(:input_hash) do
        data_for_last_value_wins
          .merge(data_for_last_real_value_wins)
          .merge(data_for_int_values)
          .merge(data_for_float_values)
          .merge(data_for_saleamout_factor_values)
          .merge(data_for_cancellation_factor_values)
      end

      subject(:processed) { processor.process(input_hash) }

      describe 'last value wins' do
        PerformanceDataValueAdjustor::LAST_VALUE_WINS.each do |column_name|
          describe(column_name) do
            it 'selected the last value' do
              expect(processed[column_name]).to eq('last value')
            end
          end
        end
      end

      describe 'last real value wins' do
        PerformanceDataValueAdjustor::LAST_REAL_VALUE_WINS.each do |column_name|
          describe(column_name) do
            it 'selected the last value' do
              expect(processed[column_name]).to eq('last value')
            end
          end
        end
      end

      describe 'int values' do
        PerformanceDataValueAdjustor::INT_VALUES.each do |column_name|
          describe(column_name) do
            it 'selected the last value' do
              expect(processed[column_name]).to eq('12')
            end
          end
        end
      end

      describe 'float values' do
        PerformanceDataValueAdjustor::FLOAT_VALUES.each do |column_name|
          describe(column_name) do
            it 'selected the last value' do
              expect(processed[column_name]).to eq('12,34')
            end
          end
        end
      end

      describe 'cancellation factor values' do
        PerformanceDataValueAdjustor::CANCELLATION_FACTOR_VALUES.each do |column_name|
          describe(column_name) do
            it 'adjusts the first value with the cancellation factor' do
              expect(processed[column_name]).to eq('3,039')
            end
          end
        end
      end

      describe 'saleamout factor values' do
        PerformanceDataValueAdjustor::SALEAMOUNT_FACTOR_VALUES.each do |column_name|
          describe(column_name) do
            it 'calculates the correct value' do
              expect(processed[column_name]).to eq('9,06375')
            end
          end
        end
      end
    end
  end
end

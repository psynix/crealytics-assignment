require 'processors/column_value_grouper'

require 'csv'

module Processors
  RSpec.describe ColumnValueGrouper do
    subject(:processor) { described_class.new }

    describe '#process' do
      let(:csv) { CSV.parse(csv_string, headers: :first_row) }

      subject(:grouped_data) { processor.process(csv) }

      context 'when all values exist' do
        let(:csv_string) do
          CSV.generate do |csv|
            csv << ['Column 1', 'Column 2']
            csv << %i(column_1_value1 column_2_value1)
            csv << %i(column_1_value2 column_2_value2)
          end
        end

        it 'returns a hash of columns' do
          expect(grouped_data['Column 1']).to eq(%w(column_1_value1 column_1_value2))
          expect(grouped_data['Column 2']).to eq(%w(column_2_value1 column_2_value2))
        end
      end

      context 'when values are missing' do
        let(:csv_string) do
          CSV.generate do |csv|
            csv << ['Column 1', 'Column 2', 'Column 3']
            csv << [nil, :column_2_value1]
            csv << [:column_1_value2, nil]
          end
        end

        it 'returns a hash of columns' do
          expect(grouped_data['Column 1']).to eq([nil, 'column_1_value2'])
          expect(grouped_data['Column 2']).to eq(['column_2_value1', nil])
          expect(grouped_data['Column 3']).to eq([nil, nil])
        end
      end
    end
  end
end

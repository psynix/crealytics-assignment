require 'processors/noop'

module Processors
  RSpec.describe Noop do
    subject(:processor) { described_class.new }

    describe '#process' do
      let(:data) { double('Important data') }

      subject(:processed_data) { processor.process(data) }

      it 'returns the data unmodified' do
        is_expected.to be(data)
      end
    end
  end
end

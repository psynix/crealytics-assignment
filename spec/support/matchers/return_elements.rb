RSpec::Matchers.define :return_elements do |*expected|
  read_elements = nil

  match do |enumerator|
    read_elements = enumerator.to_a
    read_elements == expected
  end

  failure_message do |enumerator|
    "expected that #{enumerator} would return #{expected.inspect}, but it returned #{read_elements.inspect}"
  end
end

require 'data_aggregator'

require 'enumerators'

RSpec.describe DataAggregator do
  let(:processor) { double('CSV Row Processors') }

  subject(:aggregator) { described_class.new(process_with: processor) }

  describe '#aggregate' do
    let(:sorted_csv_enum) { double('Sorted CSV enumerator') }
    let(:csv_instance)    { instance_double(CSV, to_enum: sorted_csv_enum) }
    let(:csv_sorter)      { instance_double(FileUtils::CsvSorter, write: csv_instance) }

    let(:combiner_enum)   { double('Combiner enumerator') }
    let(:combiner)        { instance_double(Enumerators::Combiner, combine: combiner_enum) }

    let(:merger_enum)     { double('Merger enumerator') }
    let(:merger)          { instance_double(Enumerators::Merger, merge: merger_enum) }

    let(:csv_batch_writer) { instance_double(FileUtils::CsvBatchWriter, write: nil) }

    let(:input_path)  { Pathname('input_path') }
    let(:output_path) { Pathname('output_path') }

    subject(:perform_aggregation) { aggregator.aggregate(input_path, output_path) }

    before do
      allow(FileUtils::CsvSorter).to receive(:new).and_return(csv_sorter)

      allow(Enumerators::Combiner).to receive(:new).and_return(combiner)
      allow(Enumerators::Merger).to receive(:new).and_return(merger)

      allow(FileUtils::CsvBatchWriter).to receive(:new).and_return(csv_batch_writer)

      perform_aggregation
    end

    describe 'sorting' do
      it 'sorts the rows' do
        expect(FileUtils::CsvSorter).to have_received(:new).with(input_path, sort_by: 'Clicks')
      end

      it 'writes the sorted rows to disk' do
        expect(csv_sorter).to have_received(:write).with(output_path, extension: 'sorted')
      end
    end

    describe 'CSV data manipulation' do
      it "combines all enumerators by '#{DataAggregator::KEYWORD_UNIQUE_ID}'" do
        expect(Enumerators::Combiner).to have_received(:new)
        expect(combiner).to have_received(:combine).with(sorted_csv_enum)
      end

      it "merges all rows together" do
        expect(Enumerators::Merger).to have_received(:new).with(processor)
        expect(merger).to have_received(:merge).with(combiner_enum)
      end
    end

    it 'writes the final data in batches to disk' do
      expect(FileUtils::CsvBatchWriter).to have_received(:new).with(merger_enum)
      expect(csv_batch_writer).to have_received(:write)
    end
  end
end

## Installation

Install dependencies:

    > bundle install

Ensure the specs pass:

    > rspec

Run the program:

    > ./bin/process

The code *still* assumes that the performance data TSV files are in `ENV['HOME']/workspace`. You can change the
search path in `bin/process`.

**NOTE:** Ruby is locked (via .ruby_version) to v2.5.1; it *should* work with earlier versions but your mileage may vary 

## Run script

Please execute `bin/process` which is an executable I've added as a convenience

## Code Explanation

This script will find and process the latest TSV data files by the given pattern (currently hardcoded 
as `/*project_2012-07-27_2012-10-10_performancedata*.txt/`).

Processing, in its previous and refactored current form, involves the following:

- Sort the input file and create a `.sorted` file as a side effect
- Adjusting any monetary values by either cancellation and/or sale amount multipliers depending on column name
- Writing the resulting data set to disk in batches of 120000 indexed the the batch number and file pattern
  e.g.

  
    project_2012-07-27_2012-10-10_performancedata_0.txt
    project_2012-07-27_2012-10-10_performancedata_1.txt
    ...
    ...
    project_2012-07-27_2012-10-10_performancedata_x.txt


NOTE: This is what is currently does as most of the additional functionality has been hardcoded

## Potential

- It appears in the past the code was used to aggregate multiple TSV files. Steps to implement that:
  - Switch the file locator to match multiple files 
  - Accept multiple files in the DataAggregator; as now, there would be a need to sort all of similarly
  - Provide the Combiner enumerators for each TSV data source

- I have added the ability to inject custom processors which should also make data adjustment easier
- Modify `bin/process` to accept options on the command line; didn't get time I'm afraid

## Fixes

- Much cleanup done as part of my refactoring effort
- Switched String and Float German number conversion mixins to use refinements instead as it doesn't pollute
  the global classes  
- Use `fail RuntimeError` instead of `throw RuntimeError`
